// [[Rcpp::depends(RcppArmadillo)]]
#include <RcppArmadillo.h>


using namespace arma;
using namespace std;



// [[Rcpp::export]]
double Rh(double h,double theta,int k, int d1, int d2, int d3, int d4) 
{
  if (d1==d2 && d2==d3 && d3==d4 && d4!=k)
  {
    return((1+(sqrt(5)*abs(h))/theta+(5*pow(h,2))/(3*pow(theta,2)))*exp(-(sqrt(5)*abs(h))/theta));
  }
  if((d1==k && d2!=k && d3!=k && d4 !=k)||(d2==k && d1!=k && d3!=k && d4 !=k) )
  {
    if (h>0)
    {
      return(exp(-(sqrt(5)*(h))/theta)*(-(5*(h))/(3*pow(theta,2))-(5*sqrt(5)*pow(h,2))/(3*pow(theta,3))));
    }
    if (h<=0)
    {
      return(exp(-(sqrt(5)*(-h))/theta)*((5*(-h))/(3*pow(theta,2))+(5*sqrt(5)*pow(h,2))/(3*pow(theta,3))));
    }
  }
  if(d1==d2 && d2==k && d3!=k && d4!=k)
  {
    if (h>0)
    {
      return(exp(-(sqrt(5)*(h))/theta)*(-5/(3*pow(theta,2))-(5*sqrt(5)*(h))/(3*pow(theta,3))+(25*pow(h,2))/(3*pow(theta,4))));
    }
    if (h<=0)
    {
      return(exp(-(sqrt(5)*(-h))/theta)*(-5/(3*pow(theta,2))-(5*sqrt(5)*(-h))/(3*pow(theta,3))+(25*pow(h,2))/(3*pow(theta,4))));
    }
  }
  
  if(d1==d2 && d2==d3 && d3==d4 && d4==k)
  {
    if (h>0)
    {
      return(exp(-(sqrt(5)*(h))/theta)*(75/(3*pow(theta,4))-(125*sqrt(5)*(h))/(3*pow(theta,5))+(125*pow(h,2))/(3*pow(theta,6))));
    }
    if (h<=0)
    {
      return(exp(-(sqrt(5)*(-h))/theta)*(75/(3*pow(theta,4))-(125*sqrt(5)*(-h))/(3*pow(theta,5))+(125*pow(h,2))/(3*pow(theta,6))));
    }
  }
  if((d1==d3 && d1==k && d2!=k && d4!=k)||(d1==d4 && d1==k && d2!=k && d3!=k)
       ||(d2==d3 && d2==k && d1!=k && d4!=k)||(d2==d4 && d2==k && d1!=k && d3!=k))
  {
    //std::cout<<"Rhxy"<<//std::endl;
    if (h>0)
    {
      return(exp(-(sqrt(5)*(h))/theta)*(5/(3*pow(theta,2))+(5*sqrt(5)*(h))/(3*pow(theta,3))-(25*pow(h,2))/(3*pow(theta,4))));
    }
    if (h<=0)
    {
      return(exp(-(sqrt(5)*(-h))/theta)*(5/(3*pow(theta,2))+(5*sqrt(5)*(-h))/(3*pow(theta,3))-(25*pow(h,2))/(3*pow(theta,4))));
    }
  }
  if((d1==d2 && d2==d3 && d3==k && d4!=k)||(d1==d2 && d2==d4 && d4==k && d3!=k))
  {
    //std::cout<<"Rhxxy"<<//std::endl;
    if (h>0)
    {
      return(exp(-(sqrt(5)*(h))/theta)*(-(75*(h))/(3*pow(theta,4))+(25*sqrt(5)*pow(h,2))/(3*pow(theta,5))));
    }
    if (h<=0)
    {
      return(exp(-(sqrt(5)*(-h))/theta)*((75*(-h))/(3*pow(theta,4))-(25*sqrt(5)*pow(h,2))/(3*pow(theta,5))));
    }
  }
  if((d3==d4 && d4==d1 && d1==k && d2!=k)||(d3==d4 && d4==d2 && d2==k && d1!=k))
  {
    ////std::cout<<"Rhxyy"<<//std::endl;
    if (h>0)
    {
      return(exp(-(sqrt(5)*(h))/theta)*((75*(h))/(3*pow(theta,4))-(25*sqrt(5)*pow(h,2))/(3*pow(theta,5))));
    }
    if (h<=0)
    {
      return(exp(-(sqrt(5)*(-h))/theta)*(-(75*(-h))/(3*pow(theta,4))+(25*sqrt(5)*pow(h,2))/(3*pow(theta,5))));
    }
  }
  if((d3==k && d2!=k && d1!=k && d4 !=k)||(d4==k && d1!=k && d3!=k && d1 !=k) )
  {
    ////std::cout<<"Rhy"<<//std::endl;
    if (h>0)
    {
      return(exp(-(sqrt(5)*(h))/theta)*((5*(h))/(3*pow(theta,2))+(5*sqrt(5)*pow(h,2))/(3*pow(theta,3))));
    }
    if (h<=0)
    {
      return(exp(-(sqrt(5)*(-h))/theta)*(-(5*(-h))/(3*pow(theta,2))-(5*sqrt(5)*pow(h,2))/(3*pow(theta,3))));
    }
  }
  if(d3==d4 && d3==k && d2!=k && d1!=k)
  {
    ////std::cout<<"Rhyy"<<//std::endl;
    if (h>0)
    {
      return(exp(-(sqrt(5)*(h))/theta)*(-5/(3*pow(theta,2))-(5*sqrt(5)*(h))/(3*pow(theta,3))+(25*pow(h,2))/(3*pow(theta,4))));
    }
    if (h<=0)
    {
      return(exp(-(sqrt(5)*(-h))/theta)*(-5/(3*pow(theta,2))-(5*sqrt(5)*(-h))/(3*pow(theta,3))+(25*pow(h,2))/(3*pow(theta,4))));
    }
  }
  ////std::cout<<"Rh"<<//std::endl;
 
  return((1+(sqrt(5)*abs(h))/theta+(5*pow(h,2))/(3*pow(theta,2)))*exp(-(sqrt(5)*abs(h))/theta));
}


////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////

// [[Rcpp::export]]
double Rdh(double h,double theta,int k, int d1, int d2, int d3, int d4)
{
  //std::cout<<"k="<<k<<" et ";
  if (d1==d2 && d2==d3 && d3==d4 && d4!=k)
  {
    //std::cout<<"Rh"<<//std::endl;
    return(((sqrt(5) * ((sqrt(5) * abs(h) + 5 * pow(h,2)/(3 *theta))/theta + 1) - sqrt(5)) * abs(h)/pow(theta,2) - 30 *(pow(h,2) * theta/pow((3 * pow(theta,2)),2))) * exp(-(sqrt(5) * (abs(h)/theta))));
  }
  if((d1==k && d2!=k && d3!=k && d4 !=k)||(d2==k && d1!=k && d3!=k && d4 !=k) )
  {
    
    if (h>0)
    {
     return(h * (exp(-(sqrt(5) * (h/theta))) *
             (theta * ( 45*sqrt(5) *(h * theta/pow((3 * pow(theta,3)),2)) + 30/pow((3 * pow(theta,2)),2)) 
                - sqrt(5) * (h * (5./3. + 5*sqrt(5) * h/(3 * theta))/pow(theta,4)))));
      }
    if (h<=0)
    {
      return(h * exp(sqrt(5) * (h/theta)) * (theta * (30/pow((3 * pow(theta,2)),2) - 
             45*sqrt(5) * (h * theta/pow((3 * pow(theta,3)),2))) - sqrt(5) * 
             (h * (5*sqrt(5) * h/(3 * theta) - 5./3.)/pow(theta,4))));
    }
  }
  if(d1==d2 && d2==k && d3!=k && d4!=k)
  {
    //std::cout<<"Rhxx"<<//std::endl;
    if (h>0)
    {
      return((sqrt(5) * (h * (h * (25 * h/(3 * theta) - (5*sqrt(5)/3))/theta - 
             (5./3.))/pow(theta,4)) + theta * (30/pow((3 * pow(theta,2)),2) + 
             h * theta * ((sqrt(5)*45)/pow((3 * pow(theta,3)),2) - 300 * (h * theta/pow(3 *pow(theta,4),2))))) * exp(-(sqrt(5) * (h/theta))));
    }
    if (h<=0)
    {
      return(exp(sqrt(5) * (h/theta)) * (theta * (30/pow((3 * pow(theta,2)),2) - 
             h * theta * ((sqrt(5)*45)/pow((3 * pow(theta,3)),2) + 300 * (h * theta/pow(3 * 
             pow(theta,4),2)))) - sqrt(5) * (h * (h * (25 * h/(3 * 
             theta) + (5*sqrt(5)/3))/theta - (5./3.))/pow(theta,4))));
    }
  }
  if(d1==d2 && d2==k && d3!=k && d4!=k)
  {
    //std::cout<<"Rhxxy"<<//std::endl;
    if (h>0)
    {
      return((sqrt(5) * (h * (h * (25 * h/(3 * theta) - (5*sqrt(5)/3))/theta - 
             (5./3.))/pow(theta,4)) + theta * (30/pow((3 * pow(theta,2)),2) + 
             h * theta * ((sqrt(5)*45)/pow((3 * pow(theta,3)),2) - 300 * (h * theta/pow(3 *pow(theta,4),2))))) * exp(-(sqrt(5) * (h/theta))));
    }
    if (h<=0)
    {
      return(exp(sqrt(5) * (h/theta)) * (theta * (30/pow((3 * pow(theta,2)),2) - 
             h * theta * ((sqrt(5)*45)/pow((3 * pow(theta,3)),2) + 300 * (h * theta/pow(3 * 
             pow(theta,4),2)))) - sqrt(5) * (h * (h * (25 * h/(3 * 
             theta) + (5*sqrt(5)/3))/theta - (5./3.))/pow(theta,4))));
    }
  }
  if(d1==d2 && d2==d3 && d3==d4 && d4==k)
  {
    //std::cout<<"Rhxxyy"<<//std::endl;
    if (h>0)
    {
      return((sqrt(5) * (h * (25 + h * (125 * h/(3 * theta) - 
             ((5*75*sqrt(5))/(3*3)))/theta)/pow(theta,6)) + pow(theta,3) * (h * theta * ((1875*sqrt(5))/pow(3 * 
             pow(theta,5),2) - 2250 * (h * theta/pow((3 * pow(theta,6)),2))) - 900/pow((3 * pow(theta,4)),2))) * 
             exp(-(sqrt(5) * (h/theta))));
    }
    if (h<=0)
    {
      return(-((sqrt(5) * (h * (25 + h * (125 * h/(3 * theta) + 
             ((5*75*sqrt(5))/(3*3)))/theta)/pow(theta,6)) + pow(theta,3) * (900/pow((3 * pow(theta,4)),2) + 
             h * theta * (2250 * (h * theta/pow((3 * pow(theta,6)),2)) +(1875*sqrt(5))/pow(3 * 
             pow(theta,5),2)))) * exp(sqrt(5) * (h/theta))));
    }
  }
  if((d1==d3 && d1==k && d2!=k && d4!=k)||(d1==d4 && d1==k && d2!=k && d3!=k)
       ||(d2==d3 && d2==k && d1!=k && d4!=k)||(d2==d4 && d2==k && d1!=k && d3!=k))
  {
    //std::cout<<"Rhxy"<<//std::endl;
      if (h>0)
      {
        return((sqrt(5) * (h * (5./3. + h * ((5*sqrt(5)/3) - 
               25 * h/(3 * theta))/theta)/pow(theta,4)) + theta * (h * theta * (300 * 
               (h * theta/pow((3 * pow(theta,4)),2)) - (45*sqrt(5))/pow((3 * pow(theta,3)),2)) - 
               30/pow((3 * pow(theta,2)),2))) * exp(-(sqrt(5) * (h/theta))));
      }
      if (h<=0)
      {
        return(exp(sqrt(5) * (h/theta)) * (theta * (h * theta * ((45*sqrt(5))/pow((3 * 
               pow(theta,3)),2) + 300 * (h * theta/pow((3 * pow(theta,4)),2))) - 30/pow((3 * pow(theta,2)),2)) - 
               sqrt(5) * (h * (5./3. - h * (25 * 
               h/(3 * theta) + (5*sqrt(5)/3))/theta)/pow(theta,4))));
      }
  }
  if((d1==d2 && d2==d3 && d3==k && d4!=k)||(d1==d2 && d2==d4 && d4==k && d3!=k))
  {
    if (h>0)
    {
      return(h * (sqrt(5) * (h * ((sqrt(5)*25) * h/(3 * theta) - 
             25)/pow(theta,6)) + pow(theta,3) * (900/pow((3 * pow(theta,4)),2) - (sqrt(5)*375) * 
             (h * theta/pow((3 * pow(theta,5)),2)))) * exp(-(sqrt(5) * 
             (h/theta))));
    }
    if (h<=0)
    {
      return(h * (sqrt(5) * (h * (25 + (sqrt(5)*25) * h/(3 * 
             theta))/pow(theta,6)) + pow(theta,3) * ((sqrt(5)*375) * (h * theta/pow(3 * 
             pow(theta,5),2)) + 900/pow((3 * pow(theta,4)),2))) * exp(sqrt(5) * 
             (h/theta)));
    }
  }
  if((d3==d4 && d4==d1 && d1==k && d2!=k)||(d3==d4 && d4==d2 && d2==k && d1!=k))
  {
    ////std::cout<<"Rhxyy"<<//std::endl;
    if (h>0)
    {
      return(h * (sqrt(5) * (h * (25 - (sqrt(5)*25) * h/(3 * 
             theta))/pow(theta,6)) + pow(theta,3) * ((sqrt(5)*375) * (h * theta/pow(3 * 
             pow(theta,5),2)) - 900/pow((3 * pow(theta,4)),2))) * exp(-(sqrt(5) * 
             (h/theta))));
    }
    if (h<=0)
    {
      return(-(h * (sqrt(5) * (h * (25 + (sqrt(5)*25) * h/(3 * 
             theta))/pow(theta,6)) + pow(theta,3) * ((sqrt(5)*375) * (h * theta/pow(3 * 
             pow(theta,5),2)) + 900/pow((3 * pow(theta,4)),2))) * exp(sqrt(5) * 
             (h/theta))));
      
    }
  }
  if((d3==k && d2!=k && d1!=k && d4 !=k)||(d4==k && d1!=k && d3!=k && d1 !=k) )
  {
    ////std::cout<<"Rhy"<<//std::endl;
    if (h>0)
    {
      return(h * (sqrt(5) * (h * (5./3. + 5*sqrt(5) * 
             h/(3 * theta))/pow(theta,4)) - theta * (45*sqrt(5) * (h * 
             theta/pow((3 * pow(theta,3)),2)) + 30/pow((3 * pow(theta,2)),2))) * exp(-(sqrt(5) * 
             (h/theta))));
    }
    if (h<=0)
    {
      return(h * exp(sqrt(5) * (h/theta)) * (theta * (45*sqrt(5) * 
             (h * theta/pow((3 * pow(theta,3)),2)) - 30/pow((3 * pow(theta,2)),2)) - sqrt(5) * 
             (h * (5./3. - 5*sqrt(5) * h/(3 * theta))/pow(theta,4))));
    }
  }
  if(d3==d4 && d3==k && d2!=k && d1!=k)
  {
    ////std::cout<<"Rhyy"<<//std::endl;
    if (h>0)
    {
      return((sqrt(5) * (h * (h * (25 * h/(3 * theta) - (5*sqrt(5)/3))/theta - 
             (5./3.))/pow(theta,4)) + theta * (30/pow((3 * pow(theta,2)),2) + 
             h * theta * ((sqrt(5)*45)/pow((3 * pow(theta,3)),2) - 300 * (h * theta/pow(3 * 
             pow(theta,4),2))))) * exp(-(sqrt(5) * (h/theta))));
    }
    if (h<=0)
    {
      return(exp(sqrt(5) * (h/theta)) * (theta * (30/pow((3 * pow(theta,2)),2) - 
             h * theta * ((sqrt(5)*45)/pow((3 * pow(theta,3)),2) + 300 * (h * theta/pow(3 * 
             pow(theta,4),2)))) - sqrt(5) * (h * (h * (25 * h/(3 * 
             theta) + (5*sqrt(5)/3))/theta - (5./3.))/pow(theta,4))));
    }
  }
  ////std::cout<<"Rh"<<//std::endl;
  return(((sqrt(5) * ((sqrt(5) * abs(h) + 5 * pow(h,2)/(3 *theta))/theta + 1) - sqrt(5)) * abs(h)/pow(theta,2) - 30 *
         (pow(h,2) * theta/pow((3 * pow(theta,2)),2))) * exp(-(sqrt(5) * 
         (abs(h)/theta))));
}


// [[Rcpp::export]]
arma::mat derive(const arma::mat& X1,const arma::mat& X2, const arma::vec& Th, const arma::mat& derivee,const arma::mat& derivee2, const int& gradient )
  {
  int p=Th.n_elem, count1=0, count2=0,nder=derivee.n_rows,nder2=derivee2.n_rows;
  double h=0, theta=0, temp=1, r=0;
  arma::mat R=zeros<mat>(nder*X1.n_rows,nder2*X2.n_rows);
  arma::vec grad=zeros<vec>(p+1);
  for ( int i=0; i<X1.n_rows; i++)
    {
    for ( int j=0; j<X2.n_rows; j++)
      {
      count1=i*nder;
      for (int d1=0; d1<derivee.n_rows; d1++)
        {
        count2=j*nder;
        for (int d2=0; d2<derivee2.n_rows; d2++)
          {
          temp=1;
          grad(0)=1;
          for (int k=1; k<p+1; k++)
            {
            h=X1(i,k-1)-X2(j,k-1);
            theta=Th(k-1);
            r=Rh(h,theta,k,derivee(d1,0),derivee(d1,1),derivee2(d2,0),derivee2(d2,1));
            temp=temp*r;
            if(r==0)
              {
              grad(k)=0;
              }
            else
              {
              grad(k)=Rdh(h,theta,k,derivee(d1,0),derivee(d1,1),derivee2(d2,0),derivee2(d2,1))/r;
              }
            }
          R(count1,count2)=temp*grad(gradient);
          //cout<<"c2="<<count2<<";"<<"d1="<<d1<<";d2="<<d2<<"  ";
          count2=count2+1;
          }
        //cout<<"c1="<<count1<<endl;
        count1=count1+1;
        }
      }
    }
  return R;
  }


// [[Rcpp::export]]
arma::mat deriveSym(const arma::mat& X1, const arma::vec& Th, const arma::mat& derivee, const int& gradient )
{
  int p=Th.n_elem, count1=0, count2=0,nder=derivee.n_rows;
  double h=0, theta=0, temp=1, r=0;
  arma::mat R=zeros<mat>(nder*X1.n_rows,nder*X1.n_rows);
  arma::vec grad=zeros<vec>(p+1);
  for ( int i=0; i<X1.n_rows; i++)
  {
    for ( int j=i; j<X1.n_rows; j++)
    {
      count1=i*nder;
      for (int d1=0; d1<derivee.n_rows; d1++)
      {
        if(i==j)
        {
          count2=count1;
          for (int d2=d1; d2<derivee.n_rows; d2++)
          {
            temp=1;
            grad(0)=1;
            for (int k=1; k<p+1; k++)
            {
              h=X1(i,k-1)-X1(j,k-1);
              theta=Th(k-1);
              r=Rh(h,theta,k,derivee(d1,0),derivee(d1,1),derivee(d2,0),derivee(d2,1));
              temp=temp*r;
              if(r==0)
              {
                grad(k)=0;
              }
              else
              {
                grad(k)=Rdh(h,theta,k,derivee(d1,0),derivee(d1,1),derivee(d2,0),derivee(d2,1))/r;
              }
            }
            R(count1,count2)=temp*grad(gradient);
            //cout<<"c2="<<count2<<";"<<"d1="<<d1<<";d2="<<d2<<"  ";
            count2=count2+1;
          }
        }
        else
          {
          count2=j*nder;
          for (int d2=0; d2<derivee.n_rows; d2++)
          {
            temp=1;
            grad(0)=1;
            for (int k=1; k<p+1; k++)
            {
              h=X1(i,k-1)-X1(j,k-1);
              theta=Th(k-1);
              r=Rh(h,theta,k,derivee(d1,0),derivee(d1,1),derivee(d2,0),derivee(d2,1));
              temp=temp*r;
              if(r==0)
              {
                grad(k)=0;
              }
              else
              {
                grad(k)=Rdh(h,theta,k,derivee(d1,0),derivee(d1,1),derivee(d2,0),derivee(d2,1))/r;
              }
            }
            R(count1,count2)=temp*grad(gradient);
            //cout<<"c2="<<count2<<";"<<"d1="<<d1<<";d2="<<d2<<"  ";
            count2=count2+1;
          }
        }
        //cout<<"c1="<<count1<<endl;
        count1=count1+1;
      }
    }
  }
  R=symmatu(R);
  return R;
}





// [[Rcpp::export]]
Rcpp::List logLikCpp(const arma::mat& Xopt,const arma::vec& Yopt, const arma::vec& theta, const arma::mat& derivee, const double& nugg )
{
  int count=0,nder=derivee.n_rows;
  double mchap=0, sigmachap=0,ll;
  arma::mat xx=zeros<mat>(nder*Xopt.n_rows,nder*Xopt.n_rows), gradk=zeros<mat>(nder*Xopt.n_rows,nder*Xopt.n_rows), U=zeros<mat>(nder*Xopt.n_rows,nder*Xopt.n_rows), R=zeros<mat>(nder*Xopt.n_rows,nder*Xopt.n_rows), Rinv=eye<mat>(nder*Xopt.n_rows,nder*Xopt.n_rows);
  arma::vec un=zeros<vec>(nder*Xopt.n_rows),x=zeros<vec>(nder*Xopt.n_rows),m=zeros<vec>(nder*Xopt.n_rows),Z2=zeros<vec>(nder*Xopt.n_rows),Z=zeros<vec>(nder*Xopt.n_rows),l=zeros<vec>(theta.n_elem);
  
  

  
  R=deriveSym(Xopt,theta,derivee,0)+nugg*Rinv; //On prend Rinv car à ce stade la matrice est l'identité
  U= chol(R);
  for ( int i=0; i<Xopt.n_rows; i++)
  {
    un(count)=1;
    for ( int j=0; j<nder; j++)
    {
      count=count+1;
    }
  }
  
  
  x=solve(U.t(),un);
  m=solve(U.t(),Yopt);
  mchap=dot(x,m)/dot(x,x);
  Z=(Yopt-mchap*un);
  m=solve(trimatl(U.t()),Z);
  sigmachap=dot(m,m)/(nder*Xopt.n_rows);
  ll=((nder*Xopt.n_rows)*(log(2*datum::pi*sigmachap)+1)+2*sum(log(diagvec(U))))/2;
  cout<<"loglik:"<<std::setprecision(20)<<ll<<endl;
  xx=solve(trimatu(U),eye(size(U)), solve_opts::fast);
  Rinv=xx*xx.t();
  m=solve(trimatu(U),m);
  xx=Rinv*Z;
  xx=xx*xx.t();
  for (int k=0; k<theta.n_elem; k++) 
  {
    gradk=deriveSym(Xopt,theta,derivee,k+1);
    l(k) =( -(accu(xx % gradk))/sigmachap+accu(Rinv % gradk))/2;
  }
  
  return Rcpp::List::create(Rcpp::Named("X") = Xopt,
                            Rcpp::Named("Y") = Yopt,
                            Rcpp::Named("derivee")=derivee,
                            Rcpp::Named("theta") = theta,
                            Rcpp::Named("sigma2") = sigmachap,
                            Rcpp::Named("m") = mchap,
                            Rcpp::Named("objective") = ll,
                            Rcpp::Named("gradient") = l,
                            Rcpp::Named("C") = R,
                            Rcpp::Named("U") = U,
                            Rcpp::Named("Cinv") = Rinv/sigmachap,
                            Rcpp::Named("Z") = Z,
                            Rcpp::Named("un") = un);
}







// [[Rcpp::export]]
double logLikCppFast(const arma::mat& Xopt,const arma::vec& Yopt, const arma::vec& theta, const arma::mat& derivee, const double& nugg)
{
  int count=0,nder=derivee.n_rows;
  double mchap=0, sigmachap=0,ll;
  arma::mat xx=zeros<mat>(nder*Xopt.n_rows,nder*Xopt.n_rows), U=zeros<mat>(nder*Xopt.n_rows,nder*Xopt.n_rows), R=zeros<mat>(nder*Xopt.n_rows,nder*Xopt.n_rows),D=eye<mat>(nder*Xopt.n_rows,nder*Xopt.n_rows);
  arma::vec un=zeros<vec>(nder*Xopt.n_rows),x=zeros<vec>(nder*Xopt.n_rows),m=zeros<vec>(nder*Xopt.n_rows),Z=zeros<vec>(nder*Xopt.n_rows),l=zeros<vec>(theta.n_elem);
  R=deriveSym(Xopt,theta,derivee,0)+nugg*D; //On prend Rinv car à ce stade la matrice est l'identité
  U= chol(R);
  for ( int i=0; i<Xopt.n_rows; i++)
  {
    un(count)=1;
    for ( int j=0; j<nder; j++)
    {
      count=count+1;
    }
  }
  
  
  x=solve(U.t(),un);
  m=solve(U.t(),Yopt);
  mchap=dot(x,m)/dot(x,x);
  Z=(Yopt-mchap*un);
  m=solve(U.t(),Z);
  sigmachap=dot(m,m)/(nder*Xopt.n_rows);
  //ll=((nder*Xopt.n_rows)*(log(2*datum::pi*sigmachap)+1)+log(det(R)))/2;
  ll=((nder*Xopt.n_rows)*(log(2*datum::pi*sigmachap)+1)+2*sum(log(diagvec(U))))/2;
    return ll;
}



// [[Rcpp::export]]
Rcpp::List logLikCppFinal(const Rcpp::List& model, const double& nugg)
{
  arma::mat derivee=model["derivee"], Xopt=model["X"],  Cinv=model["Cinv"],C=model["C"],U=model["U"], D=eye<mat>(derivee.n_rows*Xopt.n_rows,derivee.n_rows*Xopt.n_rows);
  arma:: vec theta=model["theta"], Z=model["Z"], un=model["un"], l=model["gradient"],Yopt=model["Y"];
  double sigma2=model["sigma2"], m=model["m"], ll=model["objective"];
  
  
  C=sigma2*deriveSym(Xopt,theta,derivee,0)+nugg*D; 
  U= chol(C);
  Z=solve(trimatl(U.t()),(Yopt-m*un));
  Cinv=U.i();
  Cinv=Cinv*Cinv.t();
  
  return Rcpp::List::create(Rcpp::Named("X") = Xopt,
                            Rcpp::Named("Y") = Yopt,
                            Rcpp::Named("derivee")=derivee,
                            Rcpp::Named("theta") = theta,
                            Rcpp::Named("sigma2") = sigma2,
                            Rcpp::Named("m") = m,
                            Rcpp::Named("un") = un,
                            Rcpp::Named("objective") = ll,
                            Rcpp::Named("gradient") = l,
                            Rcpp::Named("Z") = Z,
                            Rcpp::Named("C") = C,
                            Rcpp::Named("Cinv") = Cinv,
                            Rcpp::Named("U") = U);
}


// [[Rcpp::export]]
Rcpp::List predCpp(const Rcpp::List& model,const arma::mat& x, const arma::mat& derivee2, const bool& variance=false, const bool& esp=false)
{
  arma::mat derivee=model["derivee"], Xopt=model["X"], Cinv=model["Cinv"],C=model["C"],U=model["U"];
  arma:: vec theta=model["theta"], Z=model["Z"], un=model["un"]; 
  double sigma2=model["sigma2"], m=model["m"];
  int nder=derivee.n_rows, nder2=derivee2.n_rows,count=0, count2=0;
  arma::mat c=zeros<mat>(nder*Xopt.n_rows,nder2),cxx=zeros<mat>(nder2,nder2),TinvZ=zeros<mat>(nder2,nder*Xopt.n_rows),TinvC=zeros<mat>(nder2,nder*Xopt.n_rows),TinvU=zeros<mat>(nder2,nder*Xopt.n_rows),ctC=zeros<mat>(nder2,nder*Xopt.n_rows),ypred=zeros<mat>(nder2,x.n_rows),s2pred=zeros<mat>(nder*x.n_rows,nder);
  arma::mat V=zeros<mat>(nder+Xopt.n_rows*nder,nder+Xopt.n_rows*nder),fM=eye<mat>(nder+Xopt.n_rows*nder,nder),s2predsub=zeros<mat>(nder2*x.n_rows,nder2);
  
  if(variance)
  {
    V.submat(nder,0,V.n_rows-1,nder-1)=repmat(eye<mat>(nder,nder),Xopt.n_rows,1);
    V.submat(0,nder,nder-1,V.n_cols-1)=repmat(eye<mat>(nder,nder),1,Xopt.n_rows);

    for ( int i=0; i<C.n_rows; i++)
    {
      for ( int k=0; k<C.n_cols; k++)
      {
        V(nder+i,nder+k)=C(i,k);
      }
    }
    V=V.i();
  }
  for ( int j=0; j<x.n_rows; j++)
  {
    arma::vec f=zeros<vec>(nder2);
    f(0)=1;
    c=sigma2*derive(Xopt, x.row(j), theta,derivee,derivee2,0); 

    TinvC=solve(trimatl(U.t()),c);
    TinvU=solve(trimatl(U.t()),un);
    TinvZ=solve(trimatl(U.t()),Z);
    ctC=c.t()*Cinv;
    //ypred.col(j) = m*f+ctC*Z;
    ypred.col(j) = m*f+TinvC.t()*Z;
    //f=f-trans(un.t()*Cinv*c);
    if(variance)
    {
      cxx=sigma2*deriveSym(x.row(j), theta,derivee,0);

      for ( int i=0; i<c.n_rows; i++)
      {
        for ( int k=0; k<c.n_cols; k++)
        {
          fM(nder+i,k)=c(i,k);
        }
      }
      s2pred.submat(j*nder,0,(j+1)*nder-1,nder-1)=cxx-fM.t()*V*fM;
      if(nder2<nder)
      {
        count=0;
        for ( int i=0; i<nder; i++)
        {
          while(count<nder2)
          {
            if(derivee(i,0)==derivee2(count,0) && derivee(i,1)==derivee2(count,1))
            {
              count2=0;
              for ( int k=0; k<nder ; k++)
              {
                while(count2<nder2)
                {
                  if(derivee(k,0)==derivee2(count2,0) && derivee(k,1)==derivee2(count2,1))
                  {
                    s2predsub(j*nder2+count,count2)=s2pred(j*nder+i,k);
                    count2=count2+1;
                  }
                }
              }
              count=count+1;
            }
          }
          
        }
      }else
      {
        s2predsub=s2pred;
      }
    }
    
    


  }
 
  return Rcpp::List::create(Rcpp::Named("m") = ypred,
                           Rcpp::Named("s2") = s2predsub);
}

// [[Rcpp::export]]
Rcpp::List predCppEy(const Rcpp::List& model,const arma::mat& x, const arma::mat& derivee2, const bool& variance=false, const bool& esp=false)
{
  arma::mat derivee=model["derivee"], Xopt=model["X"], Cinv=model["Cinv"],C=model["C"],U=model["U"], B=model["B"] ;
  arma:: vec theta=model["theta"], Z=model["Z"], un=model["un"]; 
  double sigma2=model["sigma2"], m=model["m"];
  int nder=derivee.n_rows, nder2=derivee2.n_rows,count=0, count2=0;
  arma::mat c=zeros<mat>(nder*Xopt.n_rows,nder2),cxx=zeros<mat>(nder2,nder2),TinvZ=zeros<mat>(nder2,nder*Xopt.n_rows),TinvC=zeros<mat>(nder2,nder*Xopt.n_rows),TinvU=zeros<mat>(nder2,nder*Xopt.n_rows),ctC=zeros<mat>(nder2,nder*Xopt.n_rows),ypred=zeros<mat>(1,x.n_rows),s2pred=zeros<mat>(x.n_rows,1);
  arma::mat V=zeros<mat>(nder+Xopt.n_rows*nder,nder+Xopt.n_rows*nder),fM=eye<mat>(nder+Xopt.n_rows*nder,nder),s2predsub=zeros<mat>(x.n_rows,1);
  
  if(variance)
  {
    V.submat(nder,0,V.n_rows-1,nder-1)=repmat(eye<mat>(nder,nder),Xopt.n_rows,1);
    V.submat(0,nder,nder-1,V.n_cols-1)=repmat(eye<mat>(nder,nder),1,Xopt.n_rows);
    
    for ( int i=0; i<C.n_rows; i++)
    {
      for ( int k=0; k<C.n_cols; k++)
      {
        V(nder+i,nder+k)=C(i,k);
      }
    }
    V=V.i();
  }
  for ( int j=0; j<x.n_rows; j++)
  {
    arma::vec f=zeros<vec>(nder2);
    f(0)=1;
    
    c=sigma2*derive(Xopt, x.row(j), theta,derivee,derivee2,0); 




    TinvC=solve(trimatl(U.t()),c);

    TinvU=solve(trimatl(U.t()),un);

    TinvZ=solve(trimatl(U.t()),Z);

    ctC=c.t()*Cinv;
    //ypred.col(j) = m*f+ctC*Z;

    ypred.col(j) = B.t() * (m*f+TinvC.t()*Z);
    //f=f-trans(un.t()*Cinv*c);
    if(variance)
    {
      cxx=sigma2*deriveSym(x.row(j), theta,derivee,0);
      
      for ( int i=0; i<c.n_rows; i++)
      {
        for ( int k=0; k<c.n_cols; k++)
        {
          fM(nder+i,k)=c(i,k);
        }
      }
      s2pred.submat(j,0,j,0)=B.t()*cxx*B-B.t()*fM.t()*V*fM*B;
      s2predsub=s2pred;
    }
    
    
  }
  
  return Rcpp::List::create(Rcpp::Named("m") = ypred,
                            Rcpp::Named("s2") = s2predsub);
}

// set seed
// [[Rcpp::export]]
void set_seed(double seed) {
  Rcpp::Environment base_env("package:base");
  Rcpp::Function set_seed_r = base_env["set.seed"];
  set_seed_r(std::floor(std::fabs(seed)));
}


// [[Rcpp::export]]
arma::cube simulCpp(const Rcpp::List& model,const arma::mat& x,const int& nsim, const double& d,const arma::mat& derivee2)
{
  arma::mat derivee=model["derivee"], Xopt=model["X"], U=model["U"], Cinv=model["Cinv"] ;
  arma:: vec theta=model["theta"], Z=model["Z"];
  double sigmachap=model["sigma2"],mchap=model["m"],scalTemp;
  int nder=derivee.n_rows,nder2=derivee2.n_rows;
  arma::mat c=zeros<mat>(nder*Xopt.n_rows,nder2),cxx=zeros<mat>(nder2,nder2),ctC=zeros<mat>(nder2,nder*Xopt.n_rows),TinvC=zeros<mat>(nder2,nder*Xopt.n_rows);
  arma::vec f=zeros<vec>(nder2),ypred=zeros<vec>(nder2);
  arma::cube W(nder2,nsim,x.n_rows,fill::zeros);

  f(0)=mchap;
  set_seed(d);
  arma::mat temp2(nder2,nsim,fill::randn);
  arma::mat tempMat(nder2,nder2,fill::zeros);
  for ( int j=0; j<x.n_rows; j++)
  {
    arma::mat temp=temp2;
    c=sigmachap*derive(Xopt, x.row(j), theta,derivee,derivee2,0);
    //ctC=c.t()*Cinv;
    TinvC=solve(trimatl(U.t()),c);
    cxx=sigmachap*deriveSym(x.row(j), theta,derivee2,0);
    cxx=cxx-(TinvC.t()*TinvC);

    scalTemp=sum(cxx.diag());
    temp=tempMat.t()*temp;
    ypred=f+ TinvC.t()*Z;
    temp.each_col() += ypred;
    W.slice(j)=temp;
  }

  return W;
}


// [[Rcpp::export]]
arma::mat critere(const arma::mat& y,const arma::mat& derivee, const arma::vec& sd)
{
  int nder=derivee.n_rows, count = 1;
  arma::mat criterion=zeros<mat>(2,y.n_cols);//
    for ( int k=0; k<y.n_cols; k++)
    {
      criterion(0,k)=criterion(0,k)+y(0,k);
      count = 1;
      for ( int i=1; i<nder; i++) // On part de 1 car la premiere ligne ne nous interesse pas
      {
        criterion(1,k)=criterion(1,k)+y(i,k)*y(i,k)*sd(i);
        if (derivee(i,0) == derivee(i,1))
        {
          criterion(0,k) = criterion(0,k)+(y(i,k)*sd(count))/2;
          count = count + 1;
        }
      }
    }
  return criterion;
}





// [[Rcpp::export]]
arma::mat critereEI(const Rcpp::List& model,const arma::mat& x,const int& nsim, const double& d, const arma::vec& sd,const arma::mat& derivee2)
{
  arma::mat derivee=model["derivee"], Xopt=model["X"], U=model["U"], Cinv=model["Cinv"] ;
  arma:: vec theta=model["theta"], Z=model["Z"];
  double sigmachap=model["sigma2"],mchap=model["m"],scalTemp;
  int nder=derivee.n_rows,nder2=derivee2.n_rows;
  bool test;
  arma::mat c=zeros<mat>(nder*Xopt.n_rows,nder2),cxx=zeros<mat>(nder2,nder2),ctC=zeros<mat>(nder2,nder*Xopt.n_rows),TinvC=zeros<mat>(nder2,nder*Xopt.n_rows),criterion=zeros<mat>(nsim,x.n_rows);
  arma::vec f=zeros<vec>(nder2),ypred=zeros<vec>(nder2),err=100000000*ones<vec>(nsim);
  
  f(0)=mchap;
  set_seed(d);
  arma::mat temp2(nder2,nsim,fill::randn);
  arma::mat tempMat(nder2,nder,fill::zeros);
  for ( int j=0; j<x.n_rows; j++)
  {
    arma::mat temp=temp2;
    c=sigmachap*derive(Xopt, x.row(j), theta,derivee,derivee2,0);
    //ctC=c.t()*Cinv;
    TinvC=solve(trimatl(U.t()),c);
    cxx=sigmachap*deriveSym(x.row(j), theta,derivee2,0);
    cxx=cxx-(TinvC.t()*TinvC);
    test=chol(tempMat,cxx,"upper");
    scalTemp=sum(cxx.diag());
    if(test && scalTemp>0.000001)
    {
      temp=tempMat.t()*temp;
      // Pas pareil que sur le git hub de DiceKriging à vérifier 
      //ypred=f+solve(trimatl(U.t()),Z);
      //ypred=f+c.t()*Cinv*Z;
      ypred=f+ TinvC.t()*Z;
      temp.each_col() += ypred;
      criterion.col(j)=(critere(temp,derivee2,sd).row(1)).t();
    }else
    {
      criterion.col(j)=err;
    }
    
  }
  
  
  return criterion;
}

// [[Rcpp::export]]
arma::mat simulCondCpp(const Rcpp::List& model,const arma::mat& x,const int& nsim, const double& d, const arma::vec& sd,const arma::mat& derivee2)
{
  arma::mat derivee=model["derivee"], Xopt=model["X"], U=model["U"], Cinv=model["Cinv"] ;
  arma:: vec theta=model["theta"], Z=model["Z"];
  double sigmachap=model["sigma2"],mchap=model["m"],scalTemp;
  int nder=derivee.n_rows, count=0;
  bool test;
  arma::mat c=zeros<mat>(nder*Xopt.n_rows,x.n_rows*nder),cxx=zeros<mat>(x.n_rows*nder,x.n_rows*nder),ctC=zeros<mat>(x.n_rows*nder,nder*Xopt.n_rows),TinvC=zeros<mat>(x.n_rows*nder,nder*Xopt.n_rows),criterion=zeros<mat>(nsim,x.n_rows);
  arma::vec f=zeros<vec>(x.n_rows*nder),ypred=zeros<vec>(x.n_rows*nder),err=100000000*ones<vec>(nsim);
  for ( int i=0; i<x.n_rows; i++)
  {
    f(count)=mchap;
    for ( int j=0; j<nder; j++)
    {
      count=count+1;
    }
  }
  set_seed(d);
  arma::mat temp2(x.n_rows*nder,nsim,fill::randn);
  arma::mat temp(x.n_rows*nder,nsim,fill::zeros);
  arma::mat tempMat(x.n_rows*nder,x.n_rows*nder,fill::zeros);
    c=sigmachap*derive(Xopt, x, theta,derivee,derivee2,0);
    //ctC=c.t()*Cinv;
    TinvC=solve(trimatl(U.t()),c);
    cxx=sigmachap*deriveSym(x, theta,derivee2,0);
    cxx=cxx-(TinvC.t()*TinvC);
    test=chol(tempMat,cxx,"upper");
    scalTemp=sum(cxx.diag());
    if(test && scalTemp>0.000001)
    {
      temp=tempMat.t()*temp2;
      ypred=f+ TinvC.t()*Z;
      temp.each_col() += ypred;
      //criterion.col(j)=critere(temp,derivee,sd);
    }
  
  
  return temp;
}

