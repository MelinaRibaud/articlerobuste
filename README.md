# ArticleRobuste
This project repository contains the R code to simulate the applications (except industrial) of the article [Ribaud, M., Blanchet-Scalliet, C., Helbert, C., & Gillot, F. Robust optimization: a kriging-based multi-objective optimization approach. Reliability Engineering & System Safety (2020). ](https://www.sciencedirect.com/science/article/abs/pii/S0951832019301516).  

This section compares the strategies on two toy functions. The toy functions are the six-hump Camel in two dimensions and the Hartmann in six dimensions. Two cases are considered depending on whether the derivatives are affordable or not. For efficiency’s sake, only three of the best strategies are applied on the Hartmann function.  

The repository *FonctionsTest* contains two repository for the two toy functions. *Camel6* contains the R code for the six-hump Camel function in two dimensions and *Hartmann* the R code for the Hartmann function in six dimensions

